const http = require('http')
const app = require('express')()
const websocketServer = require('websocket').server
const httpServer = http.createServer()
const {nanoid} = require('nanoid')

// express serves index.html
app.listen('9092', () => console.log('Listening on http port 9092'))
app.get('/', (req, res) => {
    res.sendFile(__dirname + '/index.html')
})


httpServer.listen(9093, () => console.log('Listening.. on 9093'))

//hashmap
const clients = {}
const games = {}

const wsServer = new websocketServer({
    'httpServer': httpServer,
})

wsServer.on('request', request => {

    // connect
    const connection = request.accept(null, request.origin)
    connection.on('open', () => console.log('opened!'))
    connection.on('close', () => console.log('closed!'))
    connection.on('message', message => {
        const result = JSON.parse(message.utf8Data)
        // I have received a message from the client
        // a user wants to create a new game
        if (result.method === 'create') {
            const clientId = result.clientId
            const gameId = nanoid()
            console.log('game successfully created with id ' + gameId)
            games[gameId] = {
                'id': gameId,
                'balls': 20,
                'clients': []
            }
            const payLoad = {
                'method': 'create',
                'game': games[gameId]
            }
            const con = clients[clientId].connection
            con.send(JSON.stringify(payLoad))
        }

        // a client wants to join
        if (result.method === 'join') {
            const clientId = result.clientId
            const gameId = result.gameId
            const game = games[gameId]
            if (game.clients.length >= 3) {
                // sorry, max players reached
                return
            }

            const color = {
                '0': 'Red',
                '1': 'Green',
                '2': 'Blue'
            }

            game.clients.push({
                'clientId': clientId,
                'color': color[game.clients.length]
            })
            // start the game
            if (game.clients.length === 3)
                updateGameState()

            const pay = {
                'method': 'join',
                'game': game
            }
            // loop through all clients and tell them that people have joined
            game.clients.forEach(c => {
                clients[c.clientId].connection.send(JSON.stringify(pay))
            })
        }

        // a user plays
        if (result.method === 'play') {
            const clientId = result.clientId
            const gameId = result.gameId
            const ballId = result.ballId
            const color = result.color
            let state = games[gameId].state
            if (!state)
                state = {}

            state[ballId] = color
            games[gameId].state = state

            const game = games[gameId]

            const payLoad = {
                'method': 'play',
                'game': game
            }
        }
    })

    // generate a new clientId
    const clientId = nanoid()
    clients[clientId] = {
        'connection': connection
    }

    const payLoad = {
        'method': 'connect',
        'clientId': clientId
    }
    // send back the client connect
    connection.send(JSON.stringify(payLoad))
})

function updateGameState() {

    for (const g of Object.keys(games)) {
        const game = games[g]
        const pLoad = {
            'method': 'update',
            'game': game
        }

        game.clients.forEach(c => {
            clients[c.clientId].connection.send(JSON.stringify(pLoad))
        })
    }

    setTimeout(updateGameState, 500)
}
